using System;

namespace Factorial
{
    class Program
    {                
        static void Main(string[] args)
        {
            Console.WriteLine("Introduzca el que factorial quiere conocer?");
            int n = Convert.ToInt32(Console.ReadLine());

            int fac = factorial(n);
            Console.WriteLine("el factorial de " + n + " es " + fac);
            Console.ReadKey();
        }

        public static int factorial(int n)
        {
            int resultado;
            if (n == 0)
            {
                resultado = 1;
            }
            else
            {
                resultado = n * factorial(n - 1);
            }
            return resultado;
        }
    }
}
